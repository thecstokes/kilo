# 4HC3 Final Project Milestone 4 Run Instructions
1. Ensure index.html, build, and libs are all within a single folder.
2. Open index.html in Google Chrome.

# User Manual
## Enrolling in a single course.
1. Enter a search term associated with the desired class (eg. "4HC3", "Computing and Software")
2. Click the plus button on the desired course within the list
3. Use the add to calendar button to add a section (right side of the list of sections)
4. Once the desired courses are viewable within the calendar, click the enroll button in the bottom right of the screen.

## Removing a course.
1. Add a course according to the instructions above.
2. Click the calendar minus button on the course section in the calendar.

## Resolving a conflict.
1. When attempting to add a course to a timeslot already filled, the application presents the user with a message below the calendar.
2. Use the preview button to reveal the exact nature of the conflict (leftmost button on each item in the section list).
3. For an example of this, use core 0 and tutorial 1 of 4HC3.