import { Course } from "Database/Models/Course";
import UEye from "UEye/UEye";

export default class ProgramYear {
    public ID : Number;
    public courses : Course[];

    constructor(init?:Partial<ProgramYear>){
        UEye.assign(this,init);
    }
}