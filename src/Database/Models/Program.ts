import ProgramYear from "Database/Models/ProgramYear";
import UEye from "UEye/UEye";

export default class Program {
    public ID : Number;
    public name : string;
    public years : ProgramYear[];

    constructor(init?:Partial<Program>){
        UEye.assign(this, init);
    }}