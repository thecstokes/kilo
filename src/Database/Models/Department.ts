import UEye from "UEye/UEye";

export default class Department {
    public ID : Number;
    public name : string;

    constructor(init?:Partial<Department>){
        UEye.assign(this,init);
    }

    // constructor(ID: number, name: string){
    //     this.ID = ID;
    //     this.Name = name;
    // }
}