import Department from "Database/Models/Department";
import UEye from "UEye/UEye";
import Section from "Database/Models/Section";

export class Course {
    public ID: number;
    public name: string;
    public code: string;
    public department: Department;
    public sections: Section[];

    constructor(init?:Partial<Course>){
        UEye.assign(this,init);
    }

    // constructor(ID: Number, name: string, code: string,  schedule: TimeSchedule, department: Department) {

    // }
}