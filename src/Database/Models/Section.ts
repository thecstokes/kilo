import ProgramYear from "Database/Models/ProgramYear";
import UEye from "UEye/UEye";
import { TimeSlot, SectionType } from "Database/Models/TimeSchedule";

export default class Section {
    public ID : number;
    public parentID : number;
    public name : string; //i.e Core 1
    public sectionType: SectionType;
    public timeslots : TimeSlot[];
    public location : string;
    public instructor: string;
    private static idcnt : number = 0; 

    public constructor(parentID : number, name: string, location: string, instructor: string, sectionType : SectionType, testslots : TimeSlot[]){
        this.ID = Section.idcnt++;
        this.parentID = parentID;
        this.name = name;
        this.sectionType = sectionType;
        this.timeslots = testslots;
        this.location = location;
        this.instructor = instructor;
    }
}

export interface Conflict {
    existingSection: Section, newSection: Section
}