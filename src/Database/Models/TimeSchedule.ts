export class TimeSlot {
    public day : Day;
    public startTime : Time;
    public duration : number;
    public endTime : Time;

    constructor (day : Day, 
        startTime : Time, duration : number){
            this.day = day;
            this.startTime = startTime;
            this.duration = duration;
            this.computeEndTime();
           
    }

    private computeEndTime(){
        var endTimeMinutes = this.startTime.minute + this.duration%60;
        var endTimeHours =  this.startTime.hour + Math.floor(this.duration/60);
        if (endTimeMinutes > 60){
            endTimeHours += 1;
        }
        endTimeMinutes = endTimeMinutes%60;
        this.endTime = new Time (endTimeHours,endTimeMinutes);
    }

}

export enum SectionType {
    Core,
    Lab,
    Tutorial
}

export enum Day {
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday
}

export class Time {
    public hour : number;
    public minute : number;

    constructor (hour : number, minute : number){
        this.hour = hour;
        this.minute = minute;
    }

    public isBefore (time : Time) :boolean {
        if (this.hour == time.hour) return this.minute < time.minute;
        return this.hour < time.hour;
    }

    public static AIsBeforeB(a : Time, b : Time) : boolean {
        if (a.hour == b.hour) return a.minute < b.minute;
        return a.hour < b.hour;
    }

}