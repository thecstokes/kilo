import { Course } from "Database/Models/course";
import { Database } from "Database/DataBase";

export default class DB {

    public static Courses: Course[] = DB.CreateDataSet<Course>(Database.courses);

    public static PendingCourses: Course[] = [];

    // public static init() {
    //     DB.Courses = DB.CreateDataSet<Course>(Database.courses);
    //     // DB.Courses = DB.CourseDataSet<Course>(Database.courses);
    // }

    private static CreateDataSet<TData>(data: any): TData[] {
        var set: TData[] = [];
        for(var key in data) {
            set.push(data[key]);
        }
        return set;
    }
}