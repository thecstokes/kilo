import { Course } from "Database/Models/course";
import Department from "Database/Models/Department";
import {TimeSlot, Day, Time, SectionType } from "Database/Models/TimeSchedule";
import Program from "Database/Models/Program";
import ProgramYear from "Database/Models/ProgramYear";
import Section from "Database/Models/Section"


export class Database {

    private static IdCnt : number = 0;

    static departments = { 
        CAS: new Department({ID: ++Database.IdCnt, name : "Computing and Software" }),
        ENG: new Department({ID: ++Database.IdCnt, name : "Engineering"}),
        ECON : new Department({ID: ++Database.IdCnt, name : "Economics"}),
        ASTRON : new Department({ID: ++Database.IdCnt, name : "Astronomy"}),
        HUMANITIES : new Department({ID: ++Database.IdCnt, name : "Humanities"})
    }

    static courses = {
        SFWRENG4HC3: new Course({
            ID: ++Database.IdCnt,
            name: "Human Computer Interfaces",
            code: "4HC3",
            sections: 
            [
                new Section(Database.IdCnt, "Core 0", "ITB A102", "A. Lenarcic", SectionType.Core, [new TimeSlot(Day.Monday, new Time(12, 30), 50), new TimeSlot(Day.Tuesday, new Time(13, 30), 50),new TimeSlot(Day.Thursday, new Time(12, 30), 50)]),
                new Section(Database.IdCnt, "Tut 0", "ITB 236", "TBD", SectionType.Tutorial, [new TimeSlot(Day.Tuesday,  new Time(10, 30), 50)]),
                new Section(Database.IdCnt, "Tut 1", "ITB 236", "TBD", SectionType.Tutorial, [new TimeSlot(Day.Tuesday,  new Time(13, 30), 50)]),
                new Section(Database.IdCnt, "Tut 2", "ITB 236", "TBD", SectionType.Tutorial, [new TimeSlot(Day.Monday,  new Time(9, 30), 50)]),
                new Section(Database.IdCnt, "Tut 3", "ITB 236", "TBD", SectionType.Tutorial, [new TimeSlot(Day.Friday,  new Time(15, 30), 50)]),
            ],
            department: Database.departments.CAS
        }),

        ENG4A03: new Course({
            ID: ++Database.IdCnt,
            name: "Ethics and Sustainability",
            code: "4A03",
            sections:
            [
                new Section(Database.IdCnt, "Core 0", "BSB 136", "Dr. Y. Seriously",SectionType.Core, [new TimeSlot(Day.Monday, new Time(19, 0), 170)]),
            ],
            department: Database.departments.ENG
        }),

        SFWRENG4AA4: new Course({
            ID: ++Database.IdCnt,
            name: "Real Time Systems",
            code: "4AA4",
            sections: 
            [
                new Section(Database.IdCnt, "Core 0", "ITB A102", "Dr W. Hu", SectionType.Core, [new TimeSlot(Day.Monday, new Time(11, 30), 50), new TimeSlot(Day.Tuesday, new Time(11, 30), 50), new TimeSlot(Day.Friday,  new Time(13, 30), 50)]),
                new Section(Database.IdCnt, "Lab 0", "ITB 235", "TBD", SectionType.Lab, [new TimeSlot(Day.Monday, new Time(14, 30), 170)]),
                new Section(Database.IdCnt, "Lab 1", "ITB 235", "TBD", SectionType.Lab, [new TimeSlot(Day.Tuesday, new Time(14, 30), 170)]),
                new Section(Database.IdCnt, "Lab 2", "ITB 235", "TBD", SectionType.Lab, [new TimeSlot(Day.Wednesday, new Time(14, 30), 170)]),
                new Section(Database.IdCnt, "Lab 3", "ITB 235", "TBD", SectionType.Lab, [new TimeSlot(Day.Thursday, new Time(14, 30), 170)]),
                new Section(Database.IdCnt, "Lab 4", "ITB 235", "TBD", SectionType.Lab, [new TimeSlot(Day.Friday, new Time(14, 30), 170)]),
            ],
            
            department: Database.departments.CAS
        }),

        SFWRENG4TB6: new Course({
            ID: ++Database.IdCnt,
            name: "Capstone",
            code: "4TB6",
            sections: 
            [
                new Section(Database.IdCnt, "Core 0", "ITB 136", "A. Wasyng", SectionType.Core, [new TimeSlot(Day.Friday, new Time(15, 30), 50), new TimeSlot(Day.Thursday, new Time(15, 30), 50), new TimeSlot(Day.Tuesday, new Time(15, 30), 50)])
            ],
            department: Database.departments.CAS
        }),

        ECON3U03: new Course({
            ID: ++Database.IdCnt,
            name: "Econometrics",
            code: "3U03",
        
            sections: 
                [
                    new Section(Database.IdCnt, "Core 0", "MDCL 1110", "H. Simpson", SectionType.Core, [new TimeSlot(Day.Wednesday, new Time(8, 30), 110), new TimeSlot(Day.Monday, new Time(13, 30), 50)]),
                    new Section(Database.IdCnt, "Core 1", "MDCL 1305", "B. Simpson", SectionType.Core, [new TimeSlot(Day.Wednesday, new Time(12, 30), 50), new TimeSlot(Day.Thursday, new Time(12, 30), 50),new TimeSlot(Day.Monday, new Time(12, 30), 50)])
                ],
            
            department: Database.departments.ECON
        }),

        SFWRENG4E03: new Course({
            ID: ++Database.IdCnt,
            name: "Performance Analysis of Computer Systems",
            code: "4E03",
            sections:
            [
                new Section(Database.IdCnt, "Core 0", "ITB A102", "A. Mortesa", SectionType.Core, [new TimeSlot(Day.Friday, new Time(9, 30), 50), new TimeSlot(Day.Wednesday, new Time(9, 30), 50), new TimeSlot(Day.Tuesday, new Time(9, 30), 50)]),
                new Section(Database.IdCnt, "Tut 0", "GSB 102", "M. Moore", SectionType.Tutorial, [new TimeSlot(Day.Monday, new Time(14, 30), 50)]),
                new Section(Database.IdCnt, "Tut 1", "BSB 120", "E. Lefort", SectionType.Tutorial, [new TimeSlot(Day.Monday, new Time(13, 30), 50)]),
                new Section(Database.IdCnt, "Tut 2", "BSB 115", "M. Moore", SectionType.Tutorial, [new TimeSlot(Day.Monday, new Time(12, 30), 50)]),
            ],
            department: Database.departments.CAS
        }),

        ASTRON2B03: new Course({
            ID: ++Database.IdCnt,
            name: "Big Questions",
            code: "2B03",
            department: Database.departments.ASTRON,
            sections:
            [
                new Section(Database.IdCnt, "Core 0", "TSH 120", "Dr. L. Parker",SectionType.Core, [new TimeSlot(Day.Monday, new Time(15, 30), 50), new TimeSlot(Day.Tuesday, new Time(15, 30), 50), new TimeSlot(Day.Thursday, new Time(15, 30), 50)]),                
                new Section(Database.IdCnt, "Tut 0", "GSB 120", "TBD", SectionType.Tutorial, [new TimeSlot(Day.Monday, new Time(8, 30), 50)]),
                new Section(Database.IdCnt, "Tut 1", "GSB 102", "TBD", SectionType.Tutorial, [new TimeSlot(Day.Monday, new Time(16, 30), 50)]),
                new Section(Database.IdCnt, "Tut 2", "TSH B108", "TBD", SectionType.Tutorial, [new TimeSlot(Day.Tuesday, new Time(8, 30), 50)]),
                new Section(Database.IdCnt, "Tut 3", "BSB 103", "TBD", SectionType.Tutorial, [new TimeSlot(Day.Tuesday, new Time(12, 30), 50)]),
                new Section(Database.IdCnt, "Tut 4", "TSH 101", "TBD", SectionType.Tutorial, [new TimeSlot(Day.Wednesday, new Time(8, 30), 50)]),
                new Section(Database.IdCnt, "Tut 5", "KTH 102", "TBD", SectionType.Tutorial, [new TimeSlot(Day.Wednesday, new Time(12, 30), 50)]),
                new Section(Database.IdCnt, "Tut 6", "KTH 102", "TBD", SectionType.Tutorial, [new TimeSlot(Day.Thursday, new Time(8, 30), 50)]),
                new Section(Database.IdCnt, "Tut 7", "ETB 101", "TBD", SectionType.Tutorial, [new TimeSlot(Day.Thursday, new Time(12, 30), 50)]),
                new Section(Database.IdCnt, "Tut 8", "ETB 111", "TBD", SectionType.Tutorial, [new TimeSlot(Day.Thursday, new Time(13, 30), 50)]),
                new Section(Database.IdCnt, "Tut 9", "Pheonix", "TBD", SectionType.Tutorial, [new TimeSlot(Day.Thursday, new Time(16, 30), 50)]),
                new Section(Database.IdCnt, "Tut 10", "IAHS 401", "TBD", SectionType.Tutorial, [new TimeSlot(Day.Friday, new Time(8, 30), 50)]),
                new Section(Database.IdCnt, "Tut 11", "MUSC 303", "TBD", SectionType.Tutorial, [new TimeSlot(Day.Friday, new Time(9, 30), 50)]),
                new Section(Database.IdCnt, "Tut 12", "CNH 122", "TBD", SectionType.Tutorial, [new TimeSlot(Day.Friday, new Time(10, 30), 50)]),
            ]
        }),

        HUM2X03: new Course({
            ID: ++Database.IdCnt,
            name: "Modern Philosophy",
            code: "2X03",
            department: Database.departments.HUMANITIES,
            sections:
            [
                new Section(Database.IdCnt, "Core 0", "TSH B108", "S. Jobs", SectionType.Core, [new TimeSlot(Day.Thursday, new Time(13, 30), 50), new TimeSlot(Day.Tuesday, new Time(13, 30), 50)]),                
                new Section(Database.IdCnt, "Tut 0", "TSH B118", "S. Wazniak", SectionType.Tutorial, [new TimeSlot(Day.Monday, new Time(14, 30), 50)]),
                new Section(Database.IdCnt, "Tut 1", "TSH B118", "B. Gates", SectionType.Tutorial, [new TimeSlot(Day.Thursday, new Time(14, 30), 50)]),
            ]
        }),

        HUM2XX3: new Course({
            ID: ++Database.IdCnt,
            name: "Modern Philosophy II",
            code: "2XX3",
            department: Database.departments.HUMANITIES,
            sections:
            [
                new Section(Database.IdCnt, "Core 0", "TSH B108", "Dr. M. Kharl", SectionType.Core, [new TimeSlot(Day.Thursday, new Time(9, 30), 50), new TimeSlot(Day.Tuesday, new Time(9, 30), 50)]),                
                new Section(Database.IdCnt, "Tut 0", "TSH B118", "TBD", SectionType.Tutorial, [new TimeSlot(Day.Monday, new Time(8, 30), 50)]),
                new Section(Database.IdCnt, "Tut 1", "CNH 102", "TBD", SectionType.Tutorial, [new TimeSlot(Day.Thursday, new Time(8, 30), 50)]),
            ]
        }),
    }

    static programs = {
        SFWRENG : new Program({
            ID : ++Database.IdCnt,
            name : "Software Engineering",
            years : [
                new ProgramYear({
                    ID: ++Database.IdCnt,
                    courses: [
                        Database.courses.SFWRENG4HC3,
                        Database.courses.SFWRENG4AA4,
                        Database.courses.SFWRENG4E03,
                        Database.courses.SFWRENG4TB6
                    ]
                })
            ]
        
        })
    } 
}