import BaseComponent from "App/Components/Core/BaseComponent";
import UEye from "UEye/UEye";
import ActionButton from "App/Components/ActionButton/ActionButton";

export default class CourseListItem extends BaseComponent {
    private _nameElement: HTMLElement;
    private _codeElement: HTMLElement;
    private _programElement: HTMLElement;

    private _code: string;
    private _name: string;
    private _program: string;
    private actionButton: ActionButton;
    
    public constructor(parent: HTMLElement) {
        super(parent, "CourseListItem");

        var content = UEye.create("div", this.element, "Content");
        var actions = UEye.create("div", this.element, "Actions");

        var codeGroup = UEye.create("div", content, "CodeGroup");
        var nameGroup = UEye.create("div", content, "NameGroup");
        var programGroup = UEye.create("div", content, "ProgramGroup");

        //Code
        this._codeElement = UEye.create("div", codeGroup, "Code");
        var codeCaption = UEye.create("div", codeGroup, "CodeCaption");
        codeCaption.textContent = "Code";

        // Name
        this._nameElement = UEye.create("div", nameGroup, "Name");
        var nameCaption = UEye.create("div", nameGroup, "NameCaption");
        nameCaption.textContent = "Name";

        // Program
        this._programElement = UEye.create("div", programGroup, "Program");
        var programCaption = UEye.create("div", programGroup, "ProgramCaption");
        programCaption.textContent = "Program";

        this.actionButton = new ActionButton(actions);
        this.actionButton.icon = "fa-plus";
        this.actionButton.tooltip = "View Course Listings"
    }

    public get code(): string {
        return this._code;
    }
    public set code(value: string) {
        this._code = value;
        this._codeElement.textContent = this._code;
    }

    public get name(): string {
        return this._name;
    }
    public set name(value: string) {
        this._name = value;
        this._nameElement.textContent = this._name;
        if(this._name.length > 17){
            this._nameElement.title = this._name;

        }
        else{
            this._nameElement.title = "";
        }
    }

    public get program(): string {
        return this._program;
    }
    public set program(value: string) {
        this._program = value;
        this._programElement.textContent = this._program;
        if(this._program.length > 16){
            this._programElement.title = this._program;
        }
        else{
            this._programElement.title = "";
        }
    }

    public get onClick(): OnClickCallback {
        return this.actionButton.onClick;
    }
    public set onClick(value: OnClickCallback) {
        this.actionButton.onClick = value;
    }
}