import UEye from "UEye/UEye";

export default class BaseComponent {
    private _element: HTMLElement;
    private _visible: boolean;

    public constructor(parent: HTMLElement, ...css: string[]) {
        this._element = UEye.create("div", parent, ...css);
    }

    public get element(): HTMLElement {
        return this._element;
    }

    public get visible(): boolean {
        return this._visible;
    }
    public set visible(value: boolean) {
        if (this._visible === value) return;
        this._visible = value;
        if (this._visible) {
            UEye.removeClass(this._element, "UEye-Invisible");
        } else {
            UEye.addClass(this._element, "UEye-Invisible");
        }
    }
}