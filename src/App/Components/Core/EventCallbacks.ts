type OnClickCallback = () => void;

type OnChangeCallback = (value: string) => void;