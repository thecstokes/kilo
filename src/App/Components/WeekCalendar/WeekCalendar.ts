import UEye from "UEye/UEye";
import BaseComponent from "App/Components/Core/BaseComponent";
import CalendarElement from "App/Components/CalendarElement/CalendarElement";

type OnEventRemoveCallback = (ID: number) => void;

export default class WeekCalendar extends BaseComponent {
    private headers: string[] = [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday"
    ];

    private times: string[] = [
        "8:00",
        "8:30",
        "9:00",
        "9:30",
        "10:00",
        "10:30",
        "11:00",
        "11:30",
        "12:00",
        "12:30",
        "13:00",
        "13:30",
        "14:00",
        "14:30",
        "15:00",
        "15:30",
        "16:00",
        "16:30",
        "17:00",
        "17:30",
        "18:00",
        "18:30",
        "19:00",
        "19:30",
        "20:00",
        "20:30",
        "21:00",
        "21:30",
        "22:00",
        "22:30",
        "23:00",
        "23:30"
    ]

    private events: CalendarElement[] = [];

    private _onEventRemove: OnEventRemoveCallback;


    public table: HTMLTableElement;
    public body: HTMLBodyElement;
    // public element: HTMLElement;

    public constructor(parent: HTMLElement) {
        super(parent, "WeekCalendar");
        // this.element = UEye.create("div", parent, "WeekCalendar");

        this.table = UEye.create("table", this.element, "Table") as HTMLTableElement;

        this.createHeaders();
        this.createBody();
        // this.addCalendarElement();

        window.addEventListener("resize", () => {
            this.body.style.height = this.element.offsetHeight + "px";
            this.body.style.width = this.element.offsetWidth + "px";
        });
    }

    private createHeaders() {
        var header = UEye.create("thead", this.table, "Header");
        var headerRow = UEye.create("tr", header, "Row");
        // Create empty space.
        UEye.create("th", headerRow, "Element");
        this.headers.forEach(h => {
            var day = UEye.create("th", headerRow, "Element");
            day.textContent = h;
        });
    }

    private createBody() {
        this.body = UEye.create("tbody", this.table, "Body") as HTMLBodyElement;
        this.body.style.height = this.element.offsetHeight + "px";
        this.body.style.width = this.element.offsetWidth + "px";

        this.times.forEach(t => {
            var rowStart = UEye.create("tr", this.body, "Row");
            // var rowEnd = UEye.create("tr", this.body, "Row");
            var time = UEye.create("td", rowStart, "TimeElement") as HTMLTableDataCellElement;
            // time.rowSpan = 2;
            time.textContent = t;

            this.headers.forEach(h => {
                var dayStart = UEye.create("td", rowStart, "Element");
                // var dayEnd = UEye.create("td", rowEnd, "Element");
            });
        });
    }

    public refreshItems(list: EventData[]) {
        this.events.forEach(e => {
            var parent: HTMLElement | null = e.element.parentElement;
            if (parent !== null) {
                var tc = parent.parentElement;
                if (tc !== null) {
                    tc.removeChild(parent);
                }
            }
        });

        this.events = [];

        list.forEach(e => {
            //Calculate which row of the table to create the element in
            var row = (e.startHour - 8) * 2 + 1;
            if (e.startMinute >= 30) row += 1;

            //Calculate which column of the table to create the element in
            var column = 1 + this.headers.indexOf(e.day);

            //Calculate the size of the element relative to the size of a cell
            var height = e.duration / 30.0;
            height = Math.floor(100 * height);
            var heightString = height.toString() + "%";

            var tableCell = this.table.rows[row].cells[column];

            //This is necessary because table cells cannnot be used with absolute positioning
            var helperDiv = UEye.create("div", tableCell, "helper");

            //Create and size the calendar element

            if (e.eventState == null) {
                e.eventState = EventState.Selected;
            }

            var color = "blue";
            var status = "";
            switch (e.eventState) {
                case EventState.Selected: {
                    color = "blue";
                    status = "Selected";
                    break;
                }
                case EventState.Preview: {
                    color = "yellow";
                    status = "Preview";
                    break;
                }
                case EventState.Saved: {
                    color = "green";
                    status = "Saved";
                    break;
                }
                case EventState.Enrolled: {
                    color = "black";
                    status = "Enrolled";
                    break;
                }
            }

            var calendarElement = new CalendarElement(helperDiv, e.courseName, e.courseCode, status);
            calendarElement.ID = e.ID;
            calendarElement.element.style.height = heightString;
            calendarElement.RemoveButton.onClick = () => this._onEventRemove(e.ID);

            calendarElement.element.style.borderColor = color;

            // if(e != null && e.isPreview == true){
            //     calendarElement.element.style.borderColor = "yellow";
            // }

            this.events.push(calendarElement);
        });

        var focusListItem = list.find(e => e.isFocus === true);
        if (focusListItem !== undefined) {
            var focusElement = this.events.find(e => e.ID === focusListItem.ID);
            if (focusElement !== undefined && focusElement.element.parentElement !== null) {
                var t = focusElement.element.parentElement.offsetTop;
                var h = focusElement.element.offsetHeight;
                this.body.scrollTop = (t - h/2);
            }
        }
    }

    public get onEventRemove(): OnEventRemoveCallback {
        return this._onEventRemove;
    }
    public set onEventRemove(value: OnEventRemoveCallback) {
        this._onEventRemove = value;
    }

    private _onEventRemoveHandler(ID: number) {
        if (this._onEventRemove !== undefined) {
            this._onEventRemove(ID);
        }
    }
}

export enum EventState {
    Preview,
    Selected,
    Saved,
    Enrolled
}

class EventData {
    public ID: number;
    public startHour: number;
    public startMinute: number;
    public duration: number;
    public day: string;
    public courseName: string;
    public courseCode: string;
    public isPreview?: boolean;
    public eventState?: EventState;
    public isFocus?: boolean
}