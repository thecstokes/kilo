import UEye from "UEye/UEye";
import Tab from "App/Components/TabLayout/Tab";
import TextActionButton from "App/Components/TextActionButton/TextActionButton";

export default class Footer {
    public text: HTMLElement;
    public saveButton: TextActionButton;
    public checkoutButton: TextActionButton;
    private infoIcon : HTMLElement;

    public constructor(parent: HTMLElement) {
        var footer = UEye.create("div", parent, "Footer");

        var MessageBox = UEye.create("div", footer, "MessageBox");
        
        this.infoIcon = UEye.create("div",MessageBox,"fa fa-info-circle");
        // infoIcon.innerHTML = "fa-info-circle"

        this.text = UEye.create("p",MessageBox,"footer-text");

        this.changeText("", "black");

        var item2 = UEye.create("div", footer, "Item");
        var item3 = UEye.create("div", footer, "Item");

        this.saveButton = new TextActionButton(item2);
        this.saveButton.text = "Save";
        this.saveButton.icon = "fa-save";

        this.checkoutButton = new TextActionButton(item3);
        this.checkoutButton.text = "Enroll";
        this.checkoutButton.icon = "fa-shopping-cart";
     }

    public changeText(text: string, color : string){
        console.log(UEye.isNullOrWhitespace(text));
        this.infoIcon.style.visibility = UEye.isNullOrWhitespace(text) ? 'hidden' : 'visible';
        this.text.style.color = color;
        this.text.innerHTML = text;
    }

}