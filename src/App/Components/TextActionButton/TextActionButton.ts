import BaseComponent from "App/Components/Core/BaseComponent";
import UEye from "UEye/UEye";

export default class TextActionButton extends BaseComponent {
    private _iconElement: HTMLElement;
    private _textElement: HTMLElement;
    private _icon: string;
    private _text: string;
    private _enabled: boolean;
    private _onClick: OnClickCallback;

    public constructor(parent: HTMLElement) {
        super(parent, "TextActionButton");
        this._textElement = UEye.create("div", this.element, "Text")
        this._iconElement = UEye.create("div", this.element, "Icon fa");   
        
        this.element.onclick = this._onClickHandler.bind(this);
    }

    public get icon(): string {
        return this._icon;
    }
    public set icon(value: string) {
        if (this._icon !== undefined) {
            UEye.removeClass(this._iconElement, this._icon);
        }
        this._icon = value;
        UEye.addClass(this._iconElement, this._icon);
    }

    public get text(): string {
        return this._text;
    }
    public set text(value: string) {
        this._textElement.textContent= value;
    }

    public get enabled(): boolean {
        return this._enabled;
    }
    public set enabled(value: boolean) {
        this._enabled = value;
        if (!this._enabled) {
            this.element.setAttribute("disabled", "true");
        } else {
            this.element.removeAttribute("disabled");
        }
    }

    public get onClick(): OnClickCallback {
        return this._onClick;
    }
    public set onClick(value: OnClickCallback) {
        this._onClick = value;
    }

    private _onClickHandler() {
        if (this._onClick !== undefined) {
            this._onClick();
        }
    }
}