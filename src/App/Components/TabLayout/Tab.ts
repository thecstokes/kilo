import UEye from "UEye/UEye";
import BaseComponent from "App/Components/Core/BaseComponent";

export default class Tab extends BaseComponent {
    // public ParentTab: HTMLElement;
    public TabText: HTMLElement;
    public DepartmentText: HTMLElement;
    public CourseCodeText: HTMLElement;
    public TabCloseButton: HTMLElement;

    private _onClick: OnClickCallback;
    private _onCloseClick: OnClickCallback;
    private _isActive: boolean;

    public constructor(TabMenu: HTMLElement, Department: string, Course: string, IsActive: boolean, HasCloseButton: boolean) 
    {
        super(TabMenu, (IsActive) ? "TabMainDiv active" : "TabMainDiv inactive")
        
        this.TabText = UEye.create("div", this.element, "TabTextDiv");                
        {
            this.DepartmentText = UEye.create("div", this.TabText, "DepartmentDiv");
            this.DepartmentText.textContent = Department;

            this.CourseCodeText = UEye.create("div", this.TabText, "CourseCodeDiv");
            this.CourseCodeText.textContent = Course;
        }
        if (HasCloseButton) {
            this.TabCloseButton = UEye.create("div", this.element, "TabCloseButton fa fa-times");
            this.TabCloseButton.onclick = this._onCloseClickHandler.bind(this);
        }

        this.element.onclick = this._onClickHandler.bind(this);
        
        //Prevents weekly schedule and no course from having tool tips
        if(HasCloseButton){
            this.TabText.title = Department + ": " + Course;
        }
        
    }

    public get isActive(): boolean {
        return this._isActive;
    }
    public set isActive(value: boolean) {
        if (this._isActive === value) return;
        this._isActive = value;
        if (value) {
            UEye.replaceClass(this.element, "inactive", "active");
        } else {
            UEye.replaceClass(this.element, "active", "inactive");
        }
    }

    public get onClick(): OnClickCallback {
        return this._onClick;
    }
    public set onClick(value: OnClickCallback) {
        this._onClick = value;
    }

    public get onCloseClick(): OnClickCallback {
        return this._onCloseClick;
    }
    public set onCloseClick(value: OnClickCallback) {
        this._onCloseClick = value;
    }

    private _onClickHandler() {
        if(this._onClick !== undefined) {
            this._onClick();
        }
    }

    private _onCloseClickHandler(e: Event) {
        if (e.stopPropagation !== undefined) {
            e.stopPropagation();
        }
        if(this._onCloseClick !== undefined) {
            this._onCloseClick();
        }
    }
}