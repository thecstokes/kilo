import UEye from "UEye/UEye";
import Tab from "App/Components/TabLayout/Tab";
import BaseComponent from "App/Components/Core/BaseComponent";

class TabGroup {
    public tab: Tab;
    public content: HTMLElement;
    public ID: number;

    public constructor(tab: Tab, content: HTMLElement) {
        this.tab = tab;
        this.content = content;
    }
}

type OnTabEventCallback = (tab: TabGroup) => void;

export default class TabLayout extends BaseComponent {
    // public content: HTMLElement;
    private TabMenu: HTMLElement;
    private Tabs: TabGroup[];
    private _onClick: OnTabEventCallback;
    private _onCloseClick: OnTabEventCallback;

    public constructor(parent: HTMLElement) {
        super(parent, "TabLayout");

        this.TabMenu = UEye.create("div", this.element, "TabMenu");
        this.Tabs = [];
    }

    public AddTab(Line1: string, Line2: string, IsActive: boolean, HasCloseButton: boolean, ID?: number): HTMLElement {
        var group = this._createTabGroup(Line1, Line2, IsActive, HasCloseButton, ID);
        this.Tabs.push(group);
        return group.content;
    }

    public refreshTabs(tabs: TabData[]) {
        this.Tabs.forEach(group => {
            var parent = group.tab.element.parentElement;
            if (parent !== null) {
                parent.removeChild(group.tab.element);
            }

            parent = group.content.parentElement;
            if (parent !== null) {
                parent.removeChild(group.content);
            }
        })
        this.Tabs = tabs.map(t => {
            var group = this._createTabGroup(t.line1, t.line2, t.isActive, t.hasCloseButton, t.ID);
            var c = t.builder(group.content);
            return group;
        })
    }

    private _createTabGroup(Line1: string, Line2: string, IsActive: boolean, HasCloseButton: boolean, ID?: number): TabGroup {
        var tab = new Tab(this.TabMenu, Line1, Line2, IsActive, HasCloseButton);
        var content = UEye.create("div", this.element, "Content");
        var group = new TabGroup(tab, content);

        if (ID !== undefined) {
            group.ID = ID;
        }

        if (IsActive) {
            UEye.addClass(content, "active");
        }
        tab.onClick = () => {
            this.Tabs.forEach(group => {
                group.tab.isActive = false;
                UEye.removeClass(group.content, "active");
            });
            tab.isActive = true;
            UEye.addClass(content, "active");
            this._onClickHandler(group);
        }
        tab.onCloseClick = () => {
            this.Tabs = this.Tabs.filter(t => {
                return (t !== group);
            });

            var parent = tab.element.parentElement;
            if (parent !== null) {
                parent.removeChild(tab.element);
            }

            parent = content.parentElement;
            if (parent !== null) {
                parent.removeChild(content);
            }

            if (this.Tabs.length > 0) {
                this.Tabs[0].tab.isActive = true;
                UEye.addClass(this.Tabs[0].content, "active");
            }

            this._onCloseClickHandler(group);
        }
        return new TabGroup(tab, content);
    }

    public get onClick(): OnTabEventCallback {
        return this._onClick;
    }
    public set onClick(value: OnTabEventCallback) {
        this._onClick = value;
    }

    public get onCloseClick(): OnTabEventCallback {
        return this._onCloseClick;
    }
    public set onCloseClick(value: OnTabEventCallback) {
        this._onCloseClick = value;
    }

    private _onCloseClickHandler(tab: TabGroup) {
        if(this._onCloseClick !== undefined) {
            this._onCloseClick(tab);
        }
    }

    private _onClickHandler(tab: TabGroup) {
        if (this._onClick !== undefined) {
            this._onClick(tab);
        }
    }
}

type ContentBuilder = (parent: HTMLElement) => any;

class TabData {
    public line1: string;
    public line2: string;
    public isActive: boolean;
    public hasCloseButton: boolean;
    public builder: ContentBuilder;
    public ID?: number;
}