import BaseComponent from "App/Components/Core/BaseComponent";
import UEye from "UEye/UEye";

export default class WaterMark extends BaseComponent {
    private _content: HTMLElement;
    private _color: string;
    private _text: string;

    public constructor(parent: HTMLElement) {
        super(parent, "WaterMark");

        this._content = UEye.create("div", this.element, "Content");
    }

    public get color(): string {
        return this._color;
    }
    public set color(value: string) {
        if (this._color === value) return;
        this._color = value;
        this._content.style.borderColor = this._color;
        this._content.style.color = this._color;
    }

    public get text(): string {
        return this._text;
    }
    public set text(value: string) {
        this._text = value;
        this._content.textContent = this._text;
    }
}