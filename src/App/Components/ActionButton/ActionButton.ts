import BaseComponent from "App/Components/Core/BaseComponent";
import UEye from "UEye/UEye";

export default class ActionButton extends BaseComponent {
    private _iconElement: HTMLElement;
    private _toolTipTextElement : HTMLElement;

    private _icon: string;
    private _onClick: OnClickCallback;
    private _enabled: boolean;
    private _toolTip: string;

    public constructor(parent: HTMLElement) {
        super(parent, "ActionButton", "tooltip");
        this._iconElement = UEye.create("div", this.element, "Icon fa");
        
        this._toolTipTextElement = UEye.create("span", this.element, "tooltiptext");
        this.element.onclick = this._onClickHandler.bind(this);
    }

    public get icon(): string {
        return this._icon;
    }
    public set icon(value: string) {
        if (this._icon !== undefined) {
            UEye.removeClass(this._iconElement, this._icon);
        }
        this._icon = value;
        UEye.addClass(this._iconElement, this._icon);
    }

    public get onClick(): OnClickCallback {
        return this._onClick;
    }
    public set onClick(value: OnClickCallback) {
        this._onClick = value;
    }

    public get enabled(): boolean {
        return this._enabled;
    }
    public set enabled(value: boolean) {
        this._enabled = value;
        if (!this._enabled) {
            this.element.setAttribute("disabled", "true");
        } else {
            this.element.removeAttribute("disabled");
        }
    }

    private _onClickHandler() {
        if(this._onClick !== undefined) {
            this._onClick();
        }
    }

    //Tool Tip
    public get tooltip() : string{
        return this._toolTip;
    }

    public set tooltip(value : string){
        this._toolTip = value;
        this._toolTipTextElement.textContent = this.tooltip;
        
    }

}