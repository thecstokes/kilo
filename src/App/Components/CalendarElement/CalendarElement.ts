import UEye from "UEye/UEye";
import BaseComponent from "App/Components/Core/BaseComponent";
import ActionButton from "App/Components/ActionButton/ActionButton";

export default class CalendarElement extends BaseComponent {
    public CalendarElement: HTMLElement;
    public CalendarItemText: HTMLElement;
    public CourseText: HTMLElement;
    public CodeText: HTMLElement;
    public ButtonDiv: HTMLElement;
    public RemoveButton: ActionButton;
    public EventStatus: HTMLElement;
    
    private _id: number;

    public constructor(parent: HTMLElement, course: string, code: string, status: string) {
        super(parent, "CalendarElement");

        // this.CalendarElement = UEye.create("div", this.element, "CalendarElement");

        this.CalendarItemText = UEye.create("div", this.element, "CalendarItemText");
        {
            this.CourseText = UEye.create("div", this.CalendarItemText, "CourseText");
            this.CourseText.textContent = course;

            this.CodeText = UEye.create("div", this.CalendarItemText, "CodeText");
            this.CodeText.textContent = code;
        }

        this.ButtonDiv = UEye.create("div", this.element, "RemoveButtonDiv");

        this.RemoveButton = new ActionButton(this.ButtonDiv);
        this.RemoveButton.icon = "fa-calendar-minus-o";
        this.RemoveButton.tooltip = "Remove From Calendar";
        this.EventStatus = UEye.create("p", this.ButtonDiv, "status-text");
        this.EventStatus.innerHTML = status;

    }

    public get ID(): number {
        return this._id;
    }
    public set ID(value: number) {
        this._id = value;
    }
}