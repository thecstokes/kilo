import BaseComponent from "App/Components/Core/BaseComponent";
import UEye from "UEye/UEye";
import ActionButton from "App/Components/ActionButton/ActionButton";

export default class SectionListItem extends BaseComponent {
    private _nameElement: HTMLElement;
    private _locationElement: HTMLElement;
    private _instructorElement: HTMLElement;

    private _name: string;
    private _location: string;
    private _instructor: string;

    private _previewButton: ActionButton;
    private _addButton: ActionButton;
    private _removeButton: ActionButton;

    private _onAddClick: OnClickCallback;
    private _onRemoveClick: OnClickCallback;
    private _onPreviewClick: OnClickCallback;

    public constructor(parent: HTMLElement) {
        super(parent, "SectionListItem");

        var content = UEye.create("div", this.element, "Content");
        var actions = UEye.create("div", this.element, "Actions");

        var nameGroup = UEye.create("div", content, "NameGroup");
        var locationGroup = UEye.create("div", content, "LocationGroup");
        var instructorGroup = UEye.create("div", content, "InstructorGroup");


        this._nameElement = UEye.create("div", nameGroup, "Name");

        var nameCaption = UEye.create("div", nameGroup, "NameCaption");
        nameCaption.textContent = "Name";

        this._locationElement = UEye.create("div", locationGroup, "Location");

        var locationCaption = UEye.create("div", locationGroup, "NameCaption");
        locationCaption.textContent = "Location";

        this._instructorElement = UEye.create("div", instructorGroup, "Instructor");

        var instructorCaption = UEye.create("div", instructorGroup, "NameCaption");
        instructorCaption.textContent = "Instructor";

        //BUTTONS
        var buttonDiv = UEye.create("div", content, "ButtonDiv");
        var previewButton = UEye.create("div", buttonDiv, "PreviewButton");
        this._previewButton = new ActionButton(previewButton);
        this._previewButton.icon = "fa-calendar";
        this._previewButton.tooltip = "Preview on Schedule";
        this._previewButton.onClick = this._onPreviewClickHandler.bind(this);

        var removeButton = UEye.create("div", buttonDiv, "RemoveButton");
        this._removeButton = new ActionButton(removeButton);
        this._removeButton.icon = "fa-calendar-minus-o";
        this._removeButton.tooltip = "Remove Item From Calendar";
        this._removeButton.onClick = this._onRemoveClickHandler.bind(this);

        var addButton = UEye.create("div", buttonDiv, "AddButton");
        this._addButton = new ActionButton(addButton);
        this._addButton.icon = "fa-calendar-plus-o";
        this._addButton.tooltip = "Add Item To Calendar";
        this._addButton.onClick = this._onAddClickHandler.bind(this);
    }

    public get name(): string {
        return this._name;
    }
    public set name(value: string) {
        this._name = value;
        this._nameElement.textContent = this._name;
    }

    public get location(): string {
        return this._location;
    }
    public set location(value: string) {
        this._location = value;
        this._locationElement.textContent = this._location;
    }

    public get instructor(): string {
        return this._instructor;
    }
    public set instructor(value: string) {
        this._instructor = value;
        this._instructorElement.textContent = this._instructor;
    }

    public get onAddClick(): OnClickCallback {
        return this._onAddClick;
    }
    public set onAddClick(value: OnClickCallback) {
        this._onAddClick = value;
    }

    public get onRemoveClick(): OnClickCallback {
        return this._onRemoveClick;
    }
    public set onRemoveClick(value: OnClickCallback) {
        this._onRemoveClick = value;
    }

    public get onPreviewClick(): OnClickCallback {
        return this._onPreviewClick;
    }
    public set onPreviewClick(value: OnClickCallback) {
        this._onPreviewClick = value;
    }

    public get canAdd(): boolean {
        return this._addButton.enabled;
    }
    public set canAdd(value: boolean) {
        this._addButton.enabled = value;
    }

    public get canRemove(): boolean {
        return this._removeButton.enabled;
    }
    public set canRemove(value: boolean) {
        this._removeButton.enabled = value;
    }

    public get canPreview(): boolean {
        return this._previewButton.enabled;
    }
    public set canPreview(value: boolean) {
        this._previewButton.enabled = value;
    }

    //Buttons
    public get addButton(): ActionButton {
        return this._addButton;
    }

    public get removeButton(): ActionButton {
        return this._removeButton;
    }

    public get previewButton(): ActionButton {
        return this._previewButton;
    }

    private _onAddClickHandler() {
        if (this._onAddClick !== undefined) {
            this._onAddClick();
        }
    }

    private _onRemoveClickHandler() {
        if (this._onRemoveClick !== undefined) {
            this._onRemoveClick();
        }
    }

    private _onPreviewClickHandler() {
        if (this._onPreviewClick !== undefined) {
            this._onPreviewClick();
        }
    }
}