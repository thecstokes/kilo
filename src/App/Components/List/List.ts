import UEye from "UEye/UEye";
import BaseComponent from "App/Components/Core/BaseComponent";

export default class List extends BaseComponent {
    private _ListItemType: { new(parent:HTMLElement): BaseComponent };
    private _listElement: HTMLElement;
    private _listItems: BaseComponent[];


    public constructor(parent: HTMLElement) {
        super(parent, "List");
        this._listElement = UEye.create("ul", this.element, "List");

        this._listItems = [];
    }

    public get ListItemType(): { new(parent:HTMLElement): BaseComponent } {
        return this._ListItemType;
    }
    public set ListItemType(value: { new(parent:HTMLElement): BaseComponent }) {
        this._ListItemType = value;
    }

    public refreshItems(items: any[]): void {
        this._listItems.forEach(item => {
            var parent = item.element.parentElement;
            if (parent !== null) {
                parent.removeChild(item.element);
            }
        });

        this._listItems = items.map(item => {
            return this._createItem(item);
        });
    }

    private _createItem(data: any) : BaseComponent {
        var listItem = new this._ListItemType(this._listElement);

        for(var key in data) {
            (listItem as any)[key] = data[key];
        }
        return listItem;
    }
}