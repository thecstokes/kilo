import UEye from "UEye/UEye";
import NavScreen from "App/Screens/Nav/NavScreen";


export default class App {
    public static main(): void {
        UEye.push(NavScreen);
    }
}