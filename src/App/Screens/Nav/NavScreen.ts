import { BaseScreen } from "UEye/BaseScreen";
import NavView from "App/Screens/Nav/NavView";
import UEye from "UEye/UEye";
import ScheduleScreen from "App/Screens/Schedule/ScheduleScreen";
import CourseSearchScreen from "App/Screens/CourseSearch/CourseSearchScreen";

export default class NavScreen extends BaseScreen<NavView> {

    public get ViewType(): new (parent: HTMLElement) => any {
        return NavView;
    }

    public onShow(): void {
        UEye.push(CourseSearchScreen, this.view.leftSide);
        UEye.push(ScheduleScreen, this.view.rightSide);
    }
}