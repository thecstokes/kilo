import UEye from "UEye/UEye";

export default class NavView {
    public leftSide: HTMLElement;
    public rightSide: HTMLElement;

    public constructor(parent: HTMLElement) {
        var NavView = UEye.create("div", parent, "NavView");

        this.leftSide = UEye.create("div", NavView, "Left");
        this.rightSide = UEye.create("div", NavView, "Right");
    }
}