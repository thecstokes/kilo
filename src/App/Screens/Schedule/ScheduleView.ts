import UEye from "UEye/UEye";
import TabLayout from "App/Components/TabLayout/TabLayout";
import WeekCalendar from "App/Components/WeekCalendar/WeekCalendar";
import List from "App/Components/List/List";
import SectionListItem from "App/Components/SectionListItem/SectionListItem";
import TextActionButton from "App/Components/TextActionButton/TextActionButton";
import Footer from "App/Components/Footer/Footer";
import CalendarElement from "App/Components/CalendarElement/CalendarElement";
import WaterMark from "App/Components/WaterMark/WaterMark";

export default class ScheduleView {
    public sectionTabLayout: TabLayout;
    public sectionList: List;
    public calendar: WeekCalendar;
    public footer: Footer;
    public sectionListWaterMark: WaterMark;

    public constructor(parent: HTMLElement) {
        var Schedule = UEye.create("div", parent, "Schedule");

        var content = UEye.create("div", Schedule, "Content");
        var bottomBar = UEye.create("div", Schedule, "BottomBar");

        var topHolder = UEye.create("div", content, "TopHolder");
        var bottomHolder = UEye.create("div", content, "BottomHolder");

        this.sectionTabLayout = new TabLayout(topHolder);
        var tabContent = this.sectionTabLayout.AddTab("No", "Course", true, false);

        this.sectionListWaterMark = new WaterMark(tabContent);
        this.sectionListWaterMark.text = "No courses added. Click + to add course.";

        var calTabLayout = new TabLayout(bottomHolder);
        var calendarContent = calTabLayout.AddTab("Weekly", "Schedule", true, false);

        this.calendar = new WeekCalendar(calendarContent);


        this.footer = new Footer(bottomBar);
        this.footer.saveButton.enabled = false;
        this.footer.checkoutButton.enabled = false;
       // var c = new CalendarElement(this.calendar.element, "Human Computer Interfaces", "4HC3");
    }
}