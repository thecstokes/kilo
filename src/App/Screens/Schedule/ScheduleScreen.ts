import { BaseScreen } from "UEye/BaseScreen";
import ScheduleView from "App/Screens/Schedule/ScheduleView";
import { StateManager, State } from "App/Screens/Schedule/StateManager";
import List from "App/Components/List/List";
import SectionListItem from "App/Components/SectionListItem/SectionListItem";
import { Day, TimeSlot } from "Database/Models/TimeSchedule";
import WaterMark from "App/Components/WaterMark/WaterMark";
import { EventState } from "App/Components/WeekCalendar/WeekCalendar";
import DB from "Database/DB";
import Section from "Database/Models/Section";

export default class ScheduleScreen extends BaseScreen<ScheduleView> {
    private stateManager: StateManager;
    private _lastAddID: number;

    public get ViewType(): new (parent: HTMLElement) => any {
        return ScheduleView;
    }

    public onShow(): void {
        this.stateManager = new StateManager();
        this.stateManager.bind(this.onRender.bind(this));

        this.view.sectionTabLayout.onClick = (tab) => {
            this.stateManager.SelectCourse.trigger({ ID: tab.ID });
        }

        this.view.sectionTabLayout.onCloseClick = (tab) => {
            this.stateManager.RemovePendingCourse.trigger({ ID: tab.ID });
        }

        this.view.calendar.onEventRemove = (ID) => {
            console.log("remove", ID);
            this.stateManager.RemoveSection.trigger({ ID: ID });
        }

        this.view.footer.saveButton.onClick = () => {
            this.stateManager.SaveSections.trigger({});
        }

        this.view.footer.checkoutButton.onClick = () => {
            this.stateManager.SaveSections.trigger({});
            this.stateManager.EnrollSections.trigger({});
        }
    }

    private onRender(original: State, current: State): void {
        console.log(current);
        var currentTabs = current.courses.map(c => {
            return {
                ID: c.ID,
                line1: c.name,
                line2: c.code,
                isActive: (current.selectedCourseID === c.ID),
                hasCloseButton: true,
                builder: (parent: HTMLElement) => {
                    var currentTabListItems = c.sections
                        .filter(section => {
                            var isSelected = (current.selectedSections.find(selectedSections => selectedSections.ID === section.ID) === undefined);
                            var isSaved = (current.savedSections.find(selectedSections => selectedSections.ID === section.ID) === undefined);
                            var isEnrolled = (current.enrolledSections.find(selectedSections => selectedSections.ID === section.ID) === undefined);
                            return (isSelected && isSaved && isEnrolled);
                        })
                        .map(s => {
                            var canPreview = (current.previewSections.find(previewSection => previewSection.ID === s.ID) === undefined);
                            var canAdd = (
                                current.selectedSections
                                    .concat(current.savedSections)
                                    .concat(current.enrolledSections)
                                    .find(selectedSection =>
                                        selectedSection.parentID === s.parentID &&
                                        selectedSection.sectionType === s.sectionType)
                                === undefined);
                            return {
                                name: s.name,
                                location: s.location,
                                instructor: s.instructor,
                                onAddClick: () => {
                                    this._lastAddID = s.ID;
                                    this.stateManager.AddSection.trigger({ section: s });
                                },
                                onRemoveClick: () => {
                                    console.log("remove");
                                    this.stateManager.RemoveSection.trigger({ ID: s.ID });
                                },
                                canPreview: canPreview,
                                canAdd: canAdd,
                                canRemove: !canPreview,
                                onPreviewClick: () => {
                                    this.stateManager.PreviewSection.trigger({ section: s });
                                }
                            }
                        });

                    console.log(currentTabListItems);
                    if (currentTabListItems.length > 0) {
                        var sectionList = new List(parent);
                        sectionList.ListItemType = SectionListItem;
                        sectionList.refreshItems(currentTabListItems);
                    } else {
                        var sectionListWaterMark = new WaterMark(parent);
                        sectionListWaterMark.text = "All sections added. Click + to add a new course.";
                    }
                }
            }
        });


        if (currentTabs.length > 0) {
            this.view.sectionTabLayout.refreshTabs(currentTabs);
        } else {
            var tabContent = this.view.sectionTabLayout.AddTab("No", "Course", true, false);

            var sectionListWaterMark = new WaterMark(tabContent);
            sectionListWaterMark.text = "No courses added. Click + to add course.";
        }

        var currentEvents = current.selectedSections
            .reduce((result, section) => {
                result = result.concat(section.timeslots.map(ts => {
                    return this._createEvent(section, ts, EventState.Selected);
                }));

                return result;
            }, new Array())
            .concat(
            current.previewSections
                .reduce((result, section) => {
                    result = result.concat(section.timeslots.map(ts => {
                        return this._createEvent(section, ts, EventState.Preview);
                    }));

                    return result;
                }, new Array())
            )
            .concat(
            current.savedSections
                .reduce((result, section) => {
                    result = result.concat(section.timeslots.map(ts => {
                        return this._createEvent(section, ts, EventState.Saved);
                    }));

                    return result;
                }, new Array())
            )
            .concat(
            current.enrolledSections
                .reduce((result, section) => {
                    result = result.concat(section.timeslots.map(ts => {
                        return this._createEvent(section, ts, EventState.Enrolled);
                    }));

                    return result;
                }, new Array())
            ).map(e => {
                if (e.ID === this._lastAddID) {
                    e.isFocus = true;
                }
                return e;
            });

        this.view.calendar.refreshItems(currentEvents);

        console.log(current.conflict);
        if (current.conflict != null) {
            var currentCourseCode: string = DB.Courses.filter(x => x.ID == current.conflict.newSection.parentID)[0].code
            var existingCourseCode: string = DB.Courses.filter(x => x.ID == current.conflict.existingSection.parentID)[0].code
            this.view.footer.changeText("Cannot add " + currentCourseCode + " "
                + current.conflict.newSection.name + " due to conflict with " + existingCourseCode + " "
                + current.conflict.existingSection.name + ". Use the preview button for more details.", "red");
        }
        else {
            this.view.footer.changeText("", "black");

        }
        this.view.footer.saveButton.enabled = (current.selectedSections.length > 0);
        this.view.footer.checkoutButton.enabled = (current.selectedSections.length > 0 || current.savedSections.length > 0);
    }

    private _createEvent(section: Section, ts: TimeSlot, eventState: any): any {
        return {
            startHour: ts.startTime.hour,
            startMinute: ts.startTime.minute,
            duration: ts.duration,
            day: Day[ts.day],
            courseName: DB.Courses.filter(x => x.ID == section.parentID)[0].name,
            courseCode: section.name,
            ID: section.ID,
            eventState: eventState
        }
    }
}