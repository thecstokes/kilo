import BaseStateManager from "UEye/BaseStateManager";
import StateBind from "UEye/StateBind";
import UEye from "UEye/UEye";
import { Course } from "Database/Models/course";
import Section, { Conflict } from "Database/Models/Section";
import { TimeSlot, Time } from "Database/Models/TimeSchedule";

export class State {
    public courses: Course[] = [];
    public selectedCourseID: number;
    public selectedSections: Section[] = [];
    public previewSections: Section[] = [];
    public savedSections: Section[] = [];
    public enrolledSections: Section[] = [];
    public conflict: Conflict | null;
}

export class StateManager extends BaseStateManager<State> {

    public constructor() {
        super(new State());

        UEye.bind("PendingCourseChange", (pendingCourse: any) => {
            this.AddPendingCourse.trigger({
                course: pendingCourse
            });
        });
    }

    public SelectCourse = new StateBind<State, {
        ID: number
    }>(this, (state, data) => {
        var next_state = UEye.clone(state);
        next_state.selectedCourseID = data.ID;
        return next_state;
    });

    public AddPendingCourse = new StateBind<State, {
        course: Course
    }>(this, (state, data) => {
        var next_state = UEye.clone(state);
        next_state.courses.push(data.course);
        next_state.selectedCourseID = data.course.ID;

        return next_state;
    });

    public RemovePendingCourse = new StateBind<State, {
        ID: number
    }>(this, (state, data) => {
        var next_state = UEye.clone(state);
        next_state.courses = next_state.courses.filter(c => {
            return (c.ID !== data.ID)
        });
        next_state.previewSections = next_state.previewSections.filter(section => section.parentID !== data.ID);
        if (next_state.courses.length > 0) {
            next_state.selectedCourseID = next_state.courses[0].ID;
        }
        UEye.trigger("RemovePendingCourse", data.ID);
        return next_state;
    });

    public AddSection = new StateBind<State, {
        section: Section
    }>(this, (state, data) => {
        var next_state = UEye.clone(state);
        var conflict = this.checkForConflicts(next_state, data.section);
        if (conflict == null) {
            next_state.selectedSections.push(data.section);
            next_state.previewSections = next_state.previewSections.filter(section => section.ID !== data.section.ID);
        }
        next_state.conflict = conflict;
        console.log(conflict);            

        return next_state;
    });

    public PreviewSection = new StateBind<State, {
        section: Section
    }>(this, (state, data) => {
        var next_state = UEye.clone(state);
        next_state.previewSections.push(data.section);
        return next_state;
    });

    public RemoveSection = new StateBind<State, {
        ID: number
    }>(this, (state, data) => {
        var next_state = UEye.clone(state);
        var section = next_state.selectedSections.find(section => section.ID === data.ID);
        next_state.selectedSections = next_state.selectedSections.filter(section => section.ID !== data.ID);
        next_state.previewSections = next_state.previewSections.filter(section => section.ID !== data.ID);
        next_state.savedSections = next_state.savedSections.filter(section => section.ID !== data.ID);
        next_state.enrolledSections = next_state.enrolledSections.filter(section => section.ID !== data.ID);
        return next_state;
    });

    public SaveSections = new StateBind<State, {}>(this, (state, data) => {
        var next_state = UEye.clone(state);
        next_state.savedSections = next_state.savedSections.concat(UEye.clone(next_state.selectedSections));
        next_state.selectedSections = [];
        return next_state;
    });

    public EnrollSections = new StateBind<State, {}>(this, (state, data) => {
        var next_state = UEye.clone(state);
        next_state.enrolledSections = next_state.enrolledSections.concat(UEye.clone(next_state.savedSections));
        next_state.savedSections = [];
        return next_state;
    });

    //Lord forgive me for I have sinned. The pyramid is real.
    //Returns the course that the new section is conflicting with.
    checkForConflicts(state: State, section: Section): Conflict | null {
        var currentSections: Section[] = state.selectedSections.concat(state.enrolledSections, state.savedSections);
        for (var currentSection of currentSections) {
            for (var existingTimeSlot of currentSection.timeslots) {
                for (var newTimeSlot of section.timeslots) {
                    if (newTimeSlot.day == existingTimeSlot.day) {
                        var earlier = Time.AIsBeforeB(existingTimeSlot.startTime, newTimeSlot.startTime) ? existingTimeSlot : newTimeSlot;
                        var later = earlier == newTimeSlot ? existingTimeSlot : newTimeSlot;
                        if (!Time.AIsBeforeB(earlier.endTime, later.startTime)) {
                            return {existingSection: currentSection, newSection: section};
                        }
                    }
                }
            }
        }
        return null;
    }

}

