import BaseStateManager from "UEye/BaseStateManager";
import { Course } from "Database/Models/course";
import StateBind from "UEye/StateBind";
import UEye from "UEye/UEye";
import { Database } from "Database/DataBase";
import DB from "Database/DB";

export class State {
    public CourseList: Course[] = [];
    public PendingCourseList: Course[] = [];
}

export class StateManager extends BaseStateManager<State> {
    public constructor() {
        super(new State());

        UEye.bind("RemovePendingCourse", (pendingCourseID: any) => {
            this.RemovePendingCourse.trigger({
                ID: pendingCourseID
            });
        });
    }

    public ResetState = new StateBind<State, {}>(this, (state, data) => {
        var next_state = UEye.clone(state);
        next_state.CourseList = UEye.clone(DB.Courses);
        return next_state;
    });

    public AddPendingCourse = new StateBind<State, {
        ID: number
    }>(this, (state, data) => {
        var next_state = UEye.clone(state);
        var course = next_state.CourseList.find(c => c.ID === data.ID);
        next_state.PendingCourseList.push(course);

        UEye.trigger("PendingCourseChange", course);

        return next_state;
    });

    public RemovePendingCourse = new StateBind<State, {
        ID: number
    }>(this, (state, data) => {
        var next_state = UEye.clone(state);
        next_state.PendingCourseList = next_state.PendingCourseList.filter(c => c.ID !== data.ID);
        return next_state;
    });

    public CourseFilterChange = new StateBind<State, {
        filter?: string
    }>(this, (state, data) => {
        var next_state = UEye.clone(state);
        if (data.filter === undefined) {
            next_state.CourseList = UEye.clone(DB.Courses);
        } else {
            var filter = data.filter.toLowerCase();
            next_state.CourseList = UEye.clone(DB.Courses).filter(c => {
                var isInName = (c.name.toLowerCase().indexOf(filter) > -1);
                var isInCode = (c.code.toLowerCase().indexOf(filter) > -1);
                var isInProgram = (c.department.name.toLowerCase().indexOf(filter) > -1);
                return (isInName || isInCode || isInProgram);
            });
        }
        return next_state;
    });
}