import UEye from "UEye/UEye";
import CourseListItem from "App/Components/CourseListItem/CourseListItem";
import List from "App/Components/List/List";
import SearchBar from "App/Components/SearchBar/SearchBar";
import WaterMark from "App/Components/WaterMark/WaterMark";

export default class CourseSearchView {
    public courseList: List;
    public searchBar: SearchBar;
    public courseListWaterMark: WaterMark;

    public constructor(parent: HTMLElement) {
        var CourseSearchView = UEye.create("div", parent, "CourseSearch");

        // var content = $ui.create("div", CourseSearchView, "content");

        this.searchBar = new SearchBar(CourseSearchView);
        this.searchBar.hint = "Course Search";

        this.courseList = new List(CourseSearchView);
        this.courseList.ListItemType = CourseListItem;
        this.courseList.refreshItems([
            {
                name: "Hello"
            },
            {
                name: "World"
            }
        ]);

        this.courseListWaterMark = new WaterMark(CourseSearchView);
        this.courseListWaterMark.visible = false;
        this.courseListWaterMark.color = "#FFFFFF";
        this.courseListWaterMark.text = "No courses found matching search.";



        // var list = UEye.create("ul", CourseSearchView, "List");

        // var item = UEye.create("li", list);
        // var l = new CourseListItem(item);
    }
}