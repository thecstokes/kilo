import { BaseScreen } from "UEye/BaseScreen";
import CourseSearchView from "App/Screens/CourseSearch/CourseSearchView";
import { StateManager, State } from "App/Screens/CourseSearch/StateManager";

export default class CourseSearchScreen extends BaseScreen<CourseSearchView> {
    private stateManager: StateManager;

    public get ViewType(): new (parent: HTMLElement) => any {
        return CourseSearchView;
    }

    public onShow(): void {
        this.stateManager = new StateManager();
        this.stateManager.bind(this.onRender.bind(this));

        this.view.searchBar.onChange = (filter) => {
            console.log(filter);
            this.stateManager.CourseFilterChange.trigger({ filter: filter });
        };

        // Reset
        this.stateManager.ResetState.trigger({});
    }

    private onRender(original: State, current: State): void {
        var currentCourseList = current.CourseList
            .filter(course => {
                return (current.PendingCourseList.find(c => c.ID === course.ID) === undefined);
            })
            .map(course => {
                return {
                    code: course.code,
                    name: course.name,
                    program: course.department.name,
                    onClick: () => {
                        console.log("onClick");
                        this.stateManager.AddPendingCourse.trigger({
                            ID: course.ID
                        })
                    }
                }
            });
        this.view.courseList.refreshItems(currentCourseList);
        this.view.courseListWaterMark.visible = (currentCourseList.length === 0);
    }
}