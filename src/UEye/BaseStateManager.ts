import UEye from "UEye/UEye";

type RenderCallback<TState> = (original: TState, current: TState) => void;

export default class BaseStateManager<TState> {
    private _renderCallbackList: RenderCallback<TState>[];
    private _current: TState;
    private _original: TState;

    public constructor(state: TState) {
        this._renderCallbackList = [];
        this._current = state;
        this._original = UEye.clone(state);
    }

    public bind(renderCallback: RenderCallback<TState>) {
        this._renderCallbackList.push(renderCallback);
    }

    public getCurrentState(): TState {
        return this._current;
    }

    public update(state: TState) {
        if (JSON.stringify(state) !== JSON.stringify(this._current)) {
            this._current = state;
        }
        this._renderCallbackList.forEach(rc => rc(
            Object.freeze(UEye.clone(this._original)), 
            Object.freeze(UEye.clone(this._current))));
    }
}