import { BaseScreen } from "UEye/BaseScreen";

declare global {
  interface Array<T> {
      find(cb: (searchElement: T, idx: number, data: T[]) => boolean) : T;
  }
}

type OnBindCallback = (data: any) => void;

export default class UEye {
    private static _parent: HTMLElement;
    private static _eventHandlers: { [key: string]: OnBindCallback } = {};

    public static init(parent: HTMLElement) {
        UEye._parent = parent;
    }

    public static push(Screen: { new(): BaseScreen<any> }, parent?: HTMLElement): any {
        if (parent === undefined) parent = UEye._parent;
        var screen = new Screen();
        var view = new screen.ViewType(parent);
        screen.view = view;
        screen.onShow();
        return view;
    }

    public static create(type: string, parent: HTMLElement, ...cssClasses: string[]): HTMLElement | HTMLInputElement {
        var element = document.createElement(type);
        UEye.addClass(element, ...cssClasses);
        parent.appendChild(element);
        return element;
    }

    public static addClass(element: HTMLElement, ...cssClasses: string[]): void {
        cssClasses.forEach(name => {
            var items = name.match(/\S+/g) || [];
            items.forEach(function (item) {
                var itemName = " " + item + " ";
                var reg = new RegExp(itemName);
                if (!reg.test(element.className)) {
                    element.className += (itemName);
                }
            });
        });
    }

    public static removeClass(element: HTMLElement, ...cssClasses: string[]): void {
        cssClasses.forEach(name => {
            var items = name.match(/\S+/g) || [];
            items.forEach(function (item) {
                var itemName = " " + item + " ";
                var reg = new RegExp(itemName);
                element.className = element.className.replace(reg, "");
            });
        });
    }

    public static replaceClass(element: HTMLElement, original: string, cssClass: string): void {
        UEye.removeClass(element, original);
        UEye.addClass(element, cssClass);
    }

    public static replaceAllClasses(element: HTMLElement, original: string[], cssClass: string[]): void {
        original.forEach(function (styleClass) {
            UEye.removeClass(element, styleClass);
        });

        cssClass.forEach(function (styleClass) {
            UEye.addClass(element, styleClass);
        });
    }

    public static isNullOrWhitespace(value: string) {
        if (value === undefined || typeof value === 'undefined' || value == null) return true;
        return value.replace(/\s/g, '').length < 1;
    }

    public static assign(target: any, varArgs: any): any {
        if (target == null) { // TypeError if undefined or null
            throw new TypeError('Cannot convert undefined or null to object');
        }

        var to = Object(target);

        for (var index = 1; index < arguments.length; index++) {
            var nextSource = arguments[index];

            if (nextSource != null) { // Skip over if undefined or null
                for (var nextKey in nextSource) {
                    // Avoid bugs when hasOwnProperty is shadowed
                    if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                        to[nextKey] = nextSource[nextKey];
                    }
                }
            }
        }
        return to;
    }

    public static clone<TData>(obj: TData): TData {
        return JSON.parse(JSON.stringify(obj));
        // var cloneObj = this.constructor();
        // for (var attribute in obj) {
        //     if (typeof obj[attribute] === "object") {
        //         cloneObj[attribute] = UEye.clone(obj[attribute]);
        //     } else {
        //         cloneObj[attribute] = obj[attribute];
        //     }
        // }
        // return cloneObj;
    }

    public static trigger(name: string, data: any): void {
        if(name in this._eventHandlers) {
            this._eventHandlers[name](data);
        }
    }

    public static bind(name: string, handler: OnBindCallback) {
        this._eventHandlers[name] = handler;
    }

}