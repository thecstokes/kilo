export abstract class BaseScreen<TView> {
    public abstract get ViewType(): { new(parent:HTMLElement): any };

    public view: TView;

    public abstract onShow(): void;
}